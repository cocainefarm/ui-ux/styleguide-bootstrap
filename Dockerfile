FROM alpine as builder

COPY . /work
WORKDIR /work
RUN apk add sassc make git
RUN make dist

FROM nginx
COPY --from=builder /work/dist/index.html /usr/share/nginx/html
COPY --from=builder /work/dist/style.css /usr/share/nginx/html
