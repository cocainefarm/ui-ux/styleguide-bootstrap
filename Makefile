

build: dist

dist: dist/index.html dist/style.css

dist/index.html:
	cp src/index.html dist/

dist/style.css: vendor/bootstrap/scss
	sassc -t compressed -I vendor/bootstrap/scss -a src/style.scss dist/style.css

vendor/bootstrap/scss:
	git submodule init
	git submodule update
